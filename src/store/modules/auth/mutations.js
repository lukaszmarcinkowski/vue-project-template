const setAuthentication = (state, is) => {
  state.isAuthenticated = is;
}

export default {
  setAuthentication
}