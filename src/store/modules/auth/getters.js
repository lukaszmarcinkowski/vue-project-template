
const isAuthenticated = state => state.isAuthenticated;
const profile = state => state.profile || {};

export default {
  isAuthenticated,
  profile
}
