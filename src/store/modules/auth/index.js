/*
 * Global Store: Auth
 */

import mutations from './mutations'
import getters from './getters'
import actions from './actions'

export default {
  namespaced: true,
  state: {
    isAuthenticated: false
  },
  getters,
  mutations,
  actions
}