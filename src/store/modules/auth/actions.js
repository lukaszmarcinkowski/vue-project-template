const auth = ({commit}, payload) => {
  commit('setAuthentication', payload);
}

export default {
  auth
}