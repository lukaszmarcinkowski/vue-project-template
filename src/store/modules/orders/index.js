/*
 * Global Store: Orders
 */

import mutations from './mutations'
import getters from './getters'
import actions from './actions'

export default {
  namespaced: true,
  state: {
    orders: []
  },
  getters,
  mutations,
  actions
}