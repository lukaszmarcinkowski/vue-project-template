import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import getters from './getters'
import actions from './actions'

// Global Store Modules
import auth from './modules/auth'
import orders from './modules/orders'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isMobile: false
  },
  getters,
  mutations,
  actions,
  modules: {
    auth,
    orders
  }
})
