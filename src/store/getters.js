const getIsMobile = state => state.isMobile;

export default {
  getIsMobile
}