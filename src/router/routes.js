import Home from '@/views/Home.vue'

const routes = [
  {
    path: '/home',
    name: 'home',
    component: Home
  },
]

export default routes;