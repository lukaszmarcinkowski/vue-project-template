import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: __URL__,
  headers: {
      'Accept' : 'application/json, text/javascript, */*; q=0.01',
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
  },
  withCredentials: true
});
instance.interceptors.response.use( function (response) {
  
  return Promise.resolve(response);
}, function (error)  {

  return Promise.reject(error);
});

export default axiosInstance;