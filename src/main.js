import Vue from 'vue'
import axios from '@/axios';
import VueAxios from 'vue-axios';
import App from './App.vue'
import store from './store'
import router from './router'

Vue.config.productionTip = false
Vue.use(VueAxios, axiosGlobal);


new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
